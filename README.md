# Requirements

 - Magento Version 2 +
 - PHP 7.0 +

# Installation

 - Download repository
 - Unzip file
 - Copy code folder from the unzip folder and paste in {magento project} / app
 - Copy moneyspace_payment.php from the unzip folder and paste in {magento project}
 - Enable Moneyspace_MoneyspacePayment & Moneyspaceqrprom_Msqrprompayment
 - Open command

**1. Enable : MoneyspacePayment (Pay by Card 3D secured)**

    php bin/magento module:enable Moneyspace_MoneyspacePayment

**2. Enable : MoneyspacePayment (QR Code Promptpay)**

    php bin/magento module:enable Moneyspaceqrprom_Msqrprompayment

**3. Enable :MoneyspacePayment (ผ่อนชำระเงิน)**

    php bin/magento module:enable Moneyspaceinstallment_Msinstallmentpayment

**4. Update Module**

    php bin/magento setup:upgrade

**5. Flush cache**

    php bin/magento cache:flush



# Configuration

 - Choose the menu [ STORES -> Configuration -> SALES -> Payment Methods ]
 - Input the Secrete ID, Secrete Key
 - Save Config



****

# Changelog

- 2024-03-10 : set default phone number to call api
- 2023-08-01 : fix php 8.2 not allow dynamic properties
- 2022-12-15 : Update payment method set order status to pending after created
- 2022-12-06 : Updated order status force to pending 
- 2021-06-10 : Updated idpay. 
- 2020-10-22 : Updated the order status system. 
- 2020-03-03 : Added disable for installment payments and fee types for credit card payment
- 2020-02-11 : Added installment payments
- 2020-02-03 : Updated QR Code Promptpay , order id , message editor (qrnone) 
- 2019-09-20 : Added
