<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Moneyspace\MoneyspacePayment\Model;



/**
 * Pay In Store payment method model
 */
class MoneyspacePayment extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'moneyspacepayment';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;


  

}
