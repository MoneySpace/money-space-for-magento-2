<?php

        if (isset($_POST["status"]) && $_POST["status"] == "OK"){

            $payment_data = array(
                'transactionID' => $_POST["transactionID"],
                'amount' => $_POST["amount"],
                'status' => $_POST["status"],
                'orderid' => $_POST["orderid"],
                'hash' => $_POST["hash"],
            );

        }else if ($_POST["status"] != "OK"){
            $payment_data = array(
                'transactionID' => $_POST["transectionID"],
                'amount' => $_POST["amount"],
                'status' => $_POST["status"],
                'orderid' => $_POST["orderid"],
                'hash' => $_POST["hash"],
            );
        }
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,url()."/rest/V1/moneyspace/payment/webhook");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);

        $data = $payment_data;

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        echo $output;
        $info = curl_getinfo($ch);
        curl_close($ch);
        echo url();
        

function url(){

    $currentPath = $_SERVER['PHP_SELF']; 

    $pathInfo = pathinfo($currentPath); 

    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'].$pathInfo['dirname'];
}

?>