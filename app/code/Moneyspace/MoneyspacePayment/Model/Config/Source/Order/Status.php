<?php

namespace Moneyspace\MoneyspacePayment\Model\Config\Source\Order;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class Status implements \Magento\Framework\Data\OptionSourceInterface {

    const UNDEFINED_OPTION_LABEL = '-- Please Select --';

    /**
     * @var string[]
     */
    protected $_stateStatuses = [
        \Magento\Sales\Model\Order::STATE_NEW,
        \Magento\Sales\Model\Order::STATE_PROCESSING,
        \Magento\Sales\Model\Order::STATE_COMPLETE,
        \Magento\Sales\Model\Order::STATE_CLOSED,
        \Magento\Sales\Model\Order::STATE_CANCELED,
        \Magento\Sales\Model\Order::STATE_HOLDED,
    ];

    /**
     * @var \Magento\Sales\Model\Order\Config
     */
    protected $_orderConfig;

    protected $_logger;

    /**
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     */
    public function __construct(\Magento\Sales\Model\Order\Config $orderConfig, \Psr\Log\LoggerInterface $logger)
    {
        $this->_orderConfig = $orderConfig;
        $this->_logger = $logger;
    }

    public function toOptionArray() {
        $statuses = $this->_stateStatuses
            ? $this->_orderConfig->getStateStatuses($this->_stateStatuses)
            : $this->_orderConfig->getStatuses();
        $options = [['value' => '', 'label' => __('-- Please Select --')]];
        foreach ($statuses as $code => $label) {
            if($code != "fraud") {
                $options[] = ['value' => $code, 'label' => $label];
            }
        }
        
        return $options;
    }

} 