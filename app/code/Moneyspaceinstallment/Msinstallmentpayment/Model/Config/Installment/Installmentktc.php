<?php
namespace Moneyspaceinstallment\Msinstallmentpayment\Model\Config\Installment;
 
class Installmentktc implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => '0', 'label' => __('ปิด')],
            ['value' => '3', 'label' => __('3 เดือน')],
            ['value' => '4', 'label' => __('4 เดือน')],
            ['value' => '5', 'label' => __('5 เดือน')],
            ['value' => '6', 'label' => __('6 เดือน')],
            ['value' => '7', 'label' => __('7 เดือน')],
            ['value' => '8', 'label' => __('8 เดือน')],
            ['value' => '9', 'label' => __('9 เดือน')],
            ['value' => '10', 'label' => __('10 เดือน')]
        ];
    }
}