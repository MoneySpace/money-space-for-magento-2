<?php

namespace Moneyspaceqrprom\Msqrprompayment\Controller\Redirect;

use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Quote\Model\Quote;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class Callback extends \Magento\Framework\App\Action\Action
{

    protected $_pageFactory;
    protected $_resultJsonFactory;
    protected $_checkoutSession;
    protected $orderRepository;
    protected $customerSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Sales\Model\Order $orderItemsDetails,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
    )
    {
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_pageFactory = $pageFactory;
        $this->orderRepository = $orderRepository;
        $this->customerSession = $customerSession;
        $this->resultRawFactory     = $resultRawFactory;
        return parent::__construct($context);
    }


    public function execute()
    {

        $customerId = $this->customerSession->getCustomer()->getId();
        $order = $this->_checkoutSession->getLastRealOrder();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderA = $objectManager->create('\Magento\Sales\Model\Order') ->load($order->getId());
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;


        /////////////////////////////////////////////////////

        $ms_secret_id = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/ms_secret_id',$storeScope);
        $ms_secret_key = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/ms_secret_key',$storeScope);
        $order_status_after_mspayment = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/order_status_after_mspayment',$storeScope);
        
        /////////////////////////////////////////////////////

        
        if ($order->getId()) {
    

                $hash = hash_hmac('sha256',$_POST["transaction_ID"].date("YmdHis"),$ms_secret_key);
                
                $payment_data = array(
                                      'secreteID' => $ms_secret_id,
                                      'transactionID' => $_POST["transaction_ID"],
                                      'timeHash' => date("YmdHis"),
                                      'hash' => $hash
                                      );
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://www.moneyspace.net/merchantapi/v1/findbytransaction/obj");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $data = $payment_data;
                
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                $output = curl_exec($ch);
                $info = curl_getinfo($ch);
                curl_close($ch);
                
                $call_dejson = json_decode($output);
                
        
                    $Amount = "Amount ";
                    $TransactionID = "Transaction ID ";
                    $StatusPayment = "Status Payment ";
                    $Description = "Description  ";
                    
                    if ($call_dejson[0]->$StatusPayment == "Pay Success") {

                        if($order_status_after_mspayment == "processing"){

                            $orderState = Order::STATE_PROCESSING;
                            $orderA->setState($orderState)->setStatus(Order::STATE_PROCESSING);
                            $orderA->save();

                        }elseif($order_status_after_mspayment == "complete"){

                            $orderState = Order::STATE_COMPLETE;
                            $orderA->setState($orderState)->setStatus(Order::STATE_COMPLETE);
                            $orderA->save();

                        }elseif($order_status_after_mspayment == "closed"){

                            $orderState = Order::STATE_CLOSED;
                            $orderA->setState($orderState)->setStatus(Order::STATE_CLOSED);
                            $orderA->save();
                            
                        }elseif($order_status_after_mspayment == "canceled"){

                            $orderState = Order::STATE_CANCELED;
                            $orderA->setState($orderState)->setStatus(Order::STATE_CANCELED);
                            $orderA->save();
                            
                        }elseif($order_status_after_mspayment == "holded"){

                            $orderState = Order::STATE_HOLDED;
                            $orderA->setState($orderState)->setStatus(Order::STATE_HOLDED);
                            $orderA->save();
                            
                        }else{

                            $orderState = Order::STATE_COMPLETE;
                            $orderA->setState($orderState)->setStatus(Order::STATE_COMPLETE);
                            $orderA->save();
                            
                        }
                        
                        $this->_checkoutSession->clearQuote();
                        $this->_checkoutSession->clearStorage();
                        $this->_checkoutSession->restoreQuote();
                        
                        $result = $this->resultRawFactory->create();
 
                        $result->setContents($output);

                        return $result;
                        
                    }else{

                        $result = $this->resultRawFactory->create();
 
                        $result->setContents($output);

                        return $result;
                    }

                }else{
                        $result = $this->resultRawFactory->create();
 
                        $result->setContents("Error !!!!!");

                        return $result;
                }


   
    }
}