<?php

namespace Moneyspaceinstallment\Msinstallmentpayment\Controller\Payment;

use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Quote\Model\Quote;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class Installment extends \Magento\Framework\App\Action\Action
{

    protected $_pageFactory;
    protected $_resultJsonFactory;
    protected $_checkoutSession;
    protected $orderRepository;
    protected $customerSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Sales\Model\Order $orderItemsDetails,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
        
    )
    {
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_pageFactory = $pageFactory;
        $this->orderRepository = $orderRepository;
        $this->customerSession = $customerSession;
        $this->resultRawFactory     = $resultRawFactory;
        return parent::__construct($context);
    }

    public function getDefaultPhoneNumber() {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $ms_secret_id = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspacepayment/ms_secret_id',$storeScope);
        $ms_secret_key = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspacepayment/ms_secret_key',$storeScope);
        $timeHash =	date("Ymdhms");
        $hash = hash_hmac("sha256", $timeHash.$ms_secret_id, $ms_secret_key);
        $data = array(
            'timeHash' => $timeHash,
            'secreteID' => $ms_secret_id,
            'hash' => $hash
        );

        $curl = curl_init();
        $query_str = http_build_query($data);
        $url = 'https://www.moneyspace.net/merchantapi/v1/store/obj?'.$query_str;
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $call_dejson = json_decode($response);
        return $call_dejson[0]->Store[0]->telephone;
    }

    public function execute()
    {

        $customerId = $this->customerSession->getCustomer()->getId();
        $order = $this->_checkoutSession->getLastRealOrder();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderA = $objectManager->create('\Magento\Sales\Model\Order') ->load($order->getId());
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;


        /////////////////////////////////////////////////////

        $ms_secret_id = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_secret_id',$storeScope);
        $ms_secret_key = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_secret_key',$storeScope);
        $order_status_after_mspayment = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/order_status_after_mspayment',$storeScope);
        $ms_fee_type = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_fee_type',$storeScope);
        $ms_fee_agreement = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_fee_agreement',$storeScope);
        $ms_bank_ktc = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_bank_ktc',$storeScope);
        $ms_bank_bay = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_bank_bay',$storeScope);
        $ms_bank_fcy = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_bank_fcy',$storeScope);

        $Endterm = "";
        $Permonths = "";
  
        if ($ms_fee_type == "include"){
            $KTC = [ 3, 4, 5, 6, 7, 8, 9, 10];
            $BAY = [ 3, 4, 6, 9, 10];
            $FCY = [ 3, 4, 6, 9, 10];
        }else if ($ms_fee_type == "exclude"){
            $KTC = [ 3, 4, 5, 6, 7, 8, 9, 10];
            $BAY = [ 3, 4, 6, 9, 10];
            $FCY = [ 3, 4, 6, 9, 10, 12, 18, 24, 36];
        }

        if ($_POST["bank"] == "KTC"){
            $Endterm = $ms_bank_ktc;
        }else if ($_POST["bank"] == "BAY"){
            $Endterm = $ms_bank_bay;
        }else if ($_POST["bank"] == "FCY"){
            $Endterm = $ms_bank_fcy;
        }


        ///////////////////////////////////////////////

        if ($order->getId() && $customerId) {

        $orderjson = date("YmdHis");

        $customer = $this->_customerFactory->create()->load($customerId);
        $firstname = $customer->getFirstname();
        $lastname = $customer->getLastname();
        $email = $customer->getEmail();
        $phone = $order->getShippingAddress()->getData("telephone") ?? $this->getDefaultPhoneNumber(); // $customer->getDefaultShippingAddress()->getTelephone();
        $total = round($order->getGrandTotal(),2);
        $currency = 'THB';
        $description = $order->getId();
        $address = "";
        $message = "";
        $feeType = $ms_fee_type;
        $order_id = $orderjson;
        $gateway_type = "card";
        $successurl = substr($this->_url->getUrl('msinstallmentpayment/redirect/callback?msref='.$orderjson), 0, -1);
        $failurl = substr($this->_url->getUrl('msinstallmentpayment/redirect/callback?msref='.$orderjson), 0, -1);
        $cancelurl = substr($this->_url->getUrl('msinstallmentpayment/redirect/callback?msref='.$orderjson), 0, -1);

        $timeHash = date("YmdHis");
        $dataHash = hash_hmac('sha256',$firstname . $lastname . $email . $phone . $total . $currency . $description . $address . $message . $feeType . $timeHash . $order_id . $gateway_type . $successurl . $failurl . $cancelurl, $ms_secret_key);
        
        $payment_data = array(
            'secreteID' => $ms_secret_id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'phone' => $phone,
            'amount' => $total,
            'currency' => $currency,
            'description' => $description,
            'address' => $address,
            'message' => $message,
            'feeType' => $feeType,
            'customer_order_id' => $order_id,
            'gatewayType' => $gateway_type,
            'timeHash' => $timeHash,
            'hash' => $dataHash,
            'successUrl' => $successurl,
            'failUrl' => $failurl,
            'cancelUrl' => $cancelurl,
            'agreement' => $ms_fee_agreement,
            'bankType' => $_POST["bank"],
            'startTerm' => "3",
            'endTerm' => $Endterm,
            'bgColor' => "#43a047",
            'txtColor' => "#FFFFFF"
            );



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://www.moneyspace.net/merchantapi/v2/createinstallment/obj");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $data = $payment_data;

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);


        
        $result = $this->resultRawFactory->create();
 
        $result->setContents($output);
 
        return $result;


        }
   
    }
}