<?php

namespace Moneyspaceqrprom\Msqrprompayment\Controller\Payment;

use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Quote\Model\Quote;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class Qrprom extends \Magento\Framework\App\Action\Action
{

    protected $_pageFactory;
    protected $_resultJsonFactory;
    protected $_checkoutSession;
    protected $orderRepository;
    protected $customerSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Sales\Model\Order $orderItemsDetails,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
        
    )
    {
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_pageFactory = $pageFactory;
        $this->orderRepository = $orderRepository;
        $this->customerSession = $customerSession;
        $this->resultRawFactory     = $resultRawFactory;
        return parent::__construct($context);
    }

    public function getDefaultPhoneNumber() {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $ms_secret_id = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspacepayment/ms_secret_id',$storeScope);
        $ms_secret_key = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspacepayment/ms_secret_key',$storeScope);
        $timeHash =	date("Ymdhms");
        $hash = hash_hmac("sha256", $timeHash.$ms_secret_id, $ms_secret_key);
        $data = array(
            'timeHash' => $timeHash,
            'secreteID' => $ms_secret_id,
            'hash' => $hash
        );

        $curl = curl_init();
        $query_str = http_build_query($data);
        $url = 'https://www.moneyspace.net/merchantapi/v1/store/obj?'.$query_str;
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $call_dejson = json_decode($response);
        return $call_dejson[0]->Store[0]->telephone;
    }

    public function execute()
    {

        $customerId = $this->customerSession->getCustomer()->getId();
        $order = $this->_checkoutSession->getLastRealOrder();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderA = $objectManager->create('\Magento\Sales\Model\Order') ->load($order->getId());
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;


        /////////////////////////////////////////////////////

        $ms_secret_id = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/ms_secret_id',$storeScope);
        $ms_secret_key = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/ms_secret_key',$storeScope);
        $title_qrscan = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/title_qrscan',$storeScope);
        $desc_qrscan = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/desc_qrscan',$storeScope);
        $order_status_after_mspayment = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/order_status_after_mspayment',$storeScope);
        

        ///////////////////////////////////////////////

        if ($order->getId() && $customerId) {

        $orderjson = date("YmdHis");

        $customer = $this->_customerFactory->create()->load($customerId);
        $firstname = $customer->getFirstname();
        $lastname = $customer->getLastname();
        $email = $customer->getEmail();
        $phone = $order->getShippingAddress()->getData("telephone") ?? $this->getDefaultPhoneNumber(); // $customer->getDefaultShippingAddress()->getTelephone();
        $total = round($order->getGrandTotal(),2);
        $currency = 'THB';
        $description = "0";
        $address = "";
        $message = "";
        $feeType = 'include';
        $order_id = $orderjson;
        $gateway_type = "qrnone";
        $successurl = $this->_url->getUrl('msqrprompayment/redirect/callback');
        $failurl = $this->_url->getUrl('msqrprompayment/redirect/callback');
        $cancelurl = $this->_url->getUrl('msqrprompayment/redirect/callback');

        $timeHash = date("YmdHis");
        $dataHash = hash_hmac('sha256',$firstname . $lastname . $email . $phone . $total . $currency . $description . $address . $message . $feeType . $timeHash . $order_id . $gateway_type . $successurl . $failurl . $cancelurl, $ms_secret_key);
        
        $payment_data = array(
            'secreteID' => $ms_secret_id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'phone' => $phone,
            'amount' => $total,
            'currency' => $currency,
            'description' => $description,
            'address' => $address,
            'message' => $message,
            'feeType' => $feeType,
            'customer_order_id' => $order_id,
            'gatewayType' => $gateway_type,
            'timeHash' => $timeHash,
            'hash' => $dataHash,
            'successUrl' => $successurl,
            'failUrl' => $failurl,
            'cancelUrl' => $cancelurl
            );



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://www.moneyspace.net/merchantapi/v2/createpayment/obj");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $data = $payment_data;

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $call_dejson = json_decode($output);

        $TransactionID = "Transaction ID";

        if($call_dejson[0]->status == "NotFound"){
            
   

        }else if($call_dejson[0]->status == "Create Success"){
            

            $hash_link = hash_hmac('sha256', $call_dejson[0]->$TransactionID.$timeHash, $ms_secret_key);
            $link = "https://www.moneyspace.net/merchantapi/makepayment/linkpaymentcard?transactionID=".$call_dejson[0]->$TransactionID."&timehash=".$timeHash."&secreteID=".$ms_secret_id."&hash=".$hash_link;


            $RedirectURL_Success = $this->_url->getUrl('checkout/onepage/success');


            $result = $this->resultRawFactory->create();
 
            $result->setContents(
            '<link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">'.'
            <link href="//cdn.muicss.com/mui-0.10.1/css/mui.min.css" rel="stylesheet" type="text/css" />
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <br>'.
            "<style>
            body{
                font-family: 'Prompt', sans-serif;
            }
            </style>
            ".'
            <div class="payment">
            <div class="mui-container-fluid">
                <div class="mui-row">
                    <div class="mui-col-md-6 mui-col-md-offset-3">
                        <div class="mui-panel" align="center">
                            <br>
                            <h3 id="scan">กรุณาสแกนชำระเงินด้วย Mobile Banking ภายในเวลา 20 นาที หลังชำระเงินกรุณารอสักครู่</h3>
                            <img id="qrpayment" src="'.$link.'">
                            <br>
                            <div id="timeout">QR Code จะสร้างใหม่ในเวลา : <span id="time" style="color:red">00:00</span></div>
                            <br>
                            <a id="newqr" href="" class="mui-btn mui-btn--primary">สร้าง QR ใหม่</a>
                            <h3 id="successpayment" style="color:green;display:none">ชำระเงินเรียบร้อยแล้ว กรุณารอสักครู่...</h3>
                            <hr>
                            <div class="mui-panel">
                                '.nl2br($desc_qrscan).'
                            </div>
                        
                        </div>
                    </div>

                </div>    
            </div>
            </div>
            <script type="text/javascript">
            $(document).ready(function () {
                var fiveMinutes = 60 * 20,
                display = document.querySelector("#time");
                startTimer(fiveMinutes, display);
                setInterval(function () {
                    $.ajax({
                        url: "'.$this->_url->getUrl('msqrprompayment/redirect/callback').'",
                        data: {"transaction_ID": "'.$call_dejson[0]->$TransactionID.'"},
                        type: "POST",
                        success: function (data, textStatus, jqXHR) {
        
                            let res = JSON.parse(data);
        
                            if (res[0]["Status Payment "] == "Pay Success"){

                                $("#scan").hide();
                                $("#qrpayment").hide();
                                $("#timeout").hide();
                                $("#newqr").hide();
                                $("#successpayment").show();
                                window.location.href = "'.$RedirectURL_Success.'"

                            }else{
                                console.log(data)
                            }
        
                        }
                    });
                }, 1000);
            });
            function startTimer(duration, display) {
                var timer = duration, minutes, seconds;
                var intervalID = setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);
            
                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
            
                    
            
                    if (--timer < 0) {
                        clearInterval(intervalID);
                        display.textContent = "กำลังสร้าง..."
                        location.reload();
                    }else{
                        display.textContent = minutes + ":" + seconds;
                    }
                }, 1000);
            }
     
            </script>
            ');
 
            return $result;

        }

    }else{
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('checkout/onepage/fail');
        return $resultRedirect;
    }

   
    }
}