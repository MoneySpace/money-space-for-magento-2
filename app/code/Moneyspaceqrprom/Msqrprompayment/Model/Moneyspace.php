<?php

/**
 * Copyright 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Moneyspaceqrprom\Msqrprompayment\Model;

use Moneyspaceqrprom\Msqrprompayment\Api\MoneyspaceInterface;
use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Quote\Model\Quote;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\InvalidRequestException;


/**
 * Defines the implementaiton class of the calculator service contract.
 */
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class Moneyspace extends \Magento\Framework\App\Action\Action implements MoneyspaceInterface
{
    protected $_pageFactory;
    protected $_resultJsonFactory;
    protected $_checkoutSession;
    protected $orderRepository;
    protected $customerSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Sales\Model\Order $orderItemsDetails,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    )
    {
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_pageFactory = $pageFactory;
        $this->orderRepository = $orderRepository;
        $this->customerSession = $customerSession;
        return parent::__construct($context);
    }
    public function execute()
    {

    }
    /**
     * Return the sum of the two numbers.
     *
     * @api
     * @return string
     */
    public function add() {


        $transactionID = $_POST["transactionID"];
        $amount = $_POST["amount"];
        $status = $_POST["status"];
        $hash = $_POST["hash"];
       
        $arr_order = json_decode($_POST["orderid"]);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderA = $objectManager->create('\Magento\Sales\Model\Order')->load($arr_order->order_id);
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;


        /////////////////////////////////////////////////////

        $ms_secret_id = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/ms_secret_id',$storeScope);
        $ms_secret_key = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/ms_secret_key',$storeScope);
        $order_status_after_mspayment = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/order_status_after_mspayment',$storeScope);    

        if ($_POST["status"] == "paysuccess" && $arr_order->payment_type == "qrprom"){


            $hashdata = hash_hmac('sha256',$transactionID.$amount.$status.$_POST["orderid"],$ms_secret_key);

                if ($hashdata == $hash){

                    if($order_status_after_mspayment == "pending"){

                        $orderState = Order::STATE_PENDING_PAYMENT;
                        $orderA->setState($orderState)->setStatus(Order::STATE_PENDING_PAYMENT);
                        $orderA->save();

                    }elseif($order_status_after_mspayment == "processing"){

                        $orderState = Order::STATE_PROCESSING;
                        $orderA->setState($orderState)->setStatus(Order::STATE_PROCESSING);
                        $orderA->save();

                    }elseif($order_status_after_mspayment == "complete"){

                        $orderState = Order::STATE_COMPLETE;
                        $orderA->setState($orderState)->setStatus(Order::STATE_COMPLETE);
                        $orderA->save();

                    }elseif($order_status_after_mspayment == "closed"){

                        $orderState = Order::STATE_CLOSED;
                        $orderA->setState($orderState)->setStatus(Order::STATE_CLOSED);
                        $orderA->save();
                        
                    }elseif($order_status_after_mspayment == "canceled"){

                        $orderState = Order::STATE_CANCELED;
                        $orderA->setState($orderState)->setStatus(Order::STATE_CANCELED);
                        $orderA->save();
                        
                    }elseif($order_status_after_mspayment == "holded"){

                        $orderState = Order::STATE_HOLDED;
                        $orderA->setState($orderState)->setStatus(Order::STATE_HOLDED);
                        $orderA->save();
                        
                    }

            }else{
                        $orderState = Order::STATE_CANCELED;
                        $orderA->setState($orderState)->setStatus(Order::STATE_CANCELED);
                        $orderA->save();
            }
            

        }else if ($_POST["status"] != "OK" && $_POST["status"] != "paysuccess"){

                        $orderState = Order::STATE_CANCELED;
                        $orderA->setState($orderState)->setStatus(Order::STATE_CANCELED);
                        $orderA->save();
            
        }

    }

}