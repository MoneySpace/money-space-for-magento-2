<?php

namespace Moneyspaceinstallment\Msinstallmentpayment\Controller\Payment;

use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Quote\Model\Quote;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class Installmentform extends \Magento\Framework\App\Action\Action
{

    protected $_pageFactory;
    protected $_resultJsonFactory;
    protected $_checkoutSession;
    protected $orderRepository;
    protected $customerSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Sales\Model\Order $orderItemsDetails,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
        
    )
    {
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_pageFactory = $pageFactory;
        $this->orderRepository = $orderRepository;
        $this->customerSession = $customerSession;
        $this->resultRawFactory     = $resultRawFactory;
        return parent::__construct($context);
    }


    public function execute()
    {

        $customerId = $this->customerSession->getCustomer()->getId();
        $order = $this->_checkoutSession->getLastRealOrder();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderA = $objectManager->create('\Magento\Sales\Model\Order') ->load($order->getId());
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;


        /////////////////////////////////////////////////////

        $ms_secret_id = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_secret_id',$storeScope);
        $ms_secret_key = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_secret_key',$storeScope);
        $order_status_after_mspayment = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/order_status_after_mspayment',$storeScope);
        $ms_fee_type = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_fee_type',$storeScope);
        $ms_fee_agreement = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_fee_agreement',$storeScope);
        $ms_bank_ktc = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_bank_ktc',$storeScope);
        $ms_bank_bay = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_bank_bay',$storeScope);
        $ms_bank_fcy = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/ms_bank_fcy',$storeScope);
        $desc_installment = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceinstallment/desc_installment',$storeScope);

        $total = $order->getGrandTotal();
        $Permonths = "";    


        if ($ms_fee_type == "include"){
            $KTC = [ 3, 4, 5, 6, 7, 8, 9, 10];
            $BAY = [ 3, 4, 6, 9, 10];
            $FCY = [ 3, 4, 6, 9, 10];
        }else if ($ms_fee_type == "exclude"){
            $KTC = [ 3, 4, 5, 6, 7, 8, 9, 10];
            $BAY = [ 3, 4, 6, 9, 10];
            $FCY = [ 3, 4, 6, 9, 10, 12, 18, 24, 36];
        }


        if ($ms_bank_ktc == '0'){
            $bank_ktc = '
            <div class="three col">
                <input type="radio" name="bank" id="ktc" value="KTC" disabled>
                <label for="ktc">
                    <img src="https://worldwallet.net/images/installment/plugins/ktc.png" alt="" style="opacity: 0.5;">
                </label>
            </div>';
        }else{
            $bank_ktc = '
            <div class="three col">
                <input type="radio" name="bank" id="ktc" value="KTC" checked>
                <label for="ktc">
                    <img src="https://worldwallet.net/images/installment/plugins/ktc.png" alt="">
                </label>
            </div>';
        }

        if ($ms_bank_bay == '0'){
            $bank_bay1 = '
            <div class="three col">
                <input type="radio" name="bank" id="bay1" value="BAY" disabled><label for="bay1">
                    <img src="https://worldwallet.net/images/installment/plugins/bay1.png" alt="" style="opacity: 0.5;">
                </label>
            </div>';
            $bank_bay2 = '
            <div class="three col">
                <input type="radio" name="bank" id="bay2" value="BAY" disabled><label for="bay2">
                    <img src="https://worldwallet.net/images/installment/plugins/bay2.png" alt="" style="opacity: 0.5;">
                </label>
            </div>';
            $bank_bay3 = '
            <div class="three col">
                <input type="radio" name="bank" id="bay3" value="BAY" disabled><label for="bay3">
                    <img src="https://worldwallet.net/images/installment/plugins/bay3.png" alt="" style="opacity: 0.5;">
                </label>
            </div>';
        }else{

            if ($ms_bank_ktc == '0'){

                $bank_bay1 = '
                <div class="three col">
                    <input type="radio" name="bank" id="bay1" value="BAY" checked><label for="bay1">
                        <img src="https://worldwallet.net/images/installment/plugins/bay1.png" alt="">
                    </label>
                </div>';

                $bank_bay2 = '
                <div class="three col">
                    <input type="radio" name="bank" id="bay2" value="BAY"><label for="bay2">
                        <img src="https://worldwallet.net/images/installment/plugins/bay2.png" alt="">
                    </label>
                </div>';
                
                $bank_bay3 = '
                <div class="three col">
                    <input type="radio" name="bank" id="bay3" value="BAY"><label for="bay3">
                        <img src="https://worldwallet.net/images/installment/plugins/bay3.png" alt="">
                    </label>
                </div>';


            }else{

                $bank_bay1 = '
                    <div class="three col">
                        <input type="radio" name="bank" id="bay1" value="BAY" checked><label for="bay1">
                            <img src="https://worldwallet.net/images/installment/plugins/bay1.png" alt="">
                        </label>
                    </div>';


                $bank_bay2 = '
                <div class="three col">
                    <input type="radio" name="bank" id="bay2" value="BAY"><label for="bay2">
                        <img src="https://worldwallet.net/images/installment/plugins/bay2.png" alt="">
                    </label>
                </div>';
                $bank_bay3 = '
                <div class="three col">
                    <input type="radio" name="bank" id="bay3" value="BAY"><label for="bay3">
                        <img src="https://worldwallet.net/images/installment/plugins/bay3.png" alt="">
                    </label>
                </div>';

            }
        }

        if ($ms_bank_fcy == '0'){
            $bank_fcy = '
            <div class="three col">
                <input type="radio" name="bank" id="fcy" value="FCY" disabled><label for="fcy">
                  <img src="https://worldwallet.net/images/installment/plugins/fcy.png" alt="" style="opacity: 0.5;">
                </label>
            </div>
            ';
        }else{

            if ($ms_bank_ktc == '0' && $ms_bank_bay == '0'){

                $bank_fcy = '
                <div class="three col">
                    <input type="radio" name="bank" id="fcy" value="FCY" checked><label for="fcy">
                    <img src="https://worldwallet.net/images/installment/plugins/fcy.png" alt="">
                    </label>
                </div>';
            }else{

                $bank_fcy = '
                <div class="three col">
                    <input type="radio" name="bank" id="fcy" value="FCY"><label for="fcy">
                    <img src="https://worldwallet.net/images/installment/plugins/fcy.png" alt="">
                    </label>
                </div>';

            }

        }

        if ($ms_bank_ktc == '0' && $ms_bank_bay == '0' && $ms_bank_fcy == '0'){
            $btn_next = '';
        }else{
            $btn_next = '<button class="button">ถัดไป</button>';
        }


        $i_select_ktc = 1;

        $select_bank_ktc = '<div class="row cf">';

        foreach($KTC as $month){

        
            if ($ms_fee_type == "include" && round($total / $month) >= 300 && $month <= $ms_bank_ktc){
                $select_bank_ktc .= '<div class="three col"><input type="hidden" id="payForMonth_ktc'.$month.'" value="'.round($total / $month,2).'"><input type="radio" name="permonth_ktc" id="perktc'.$month.'" value="'.$month.'" ><label for="perktc'.$month.'"><h4>'.number_format($total / $month,2).'  บาท / เดือน</h4></label></div>';
                if ($i_select_ktc == 4){
                    $select_bank_ktc .= '</div><div class="row cf">';
                    $i_select_ktc = 0;
                }
                $i_select_ktc++;
            }else if ($ms_fee_type == "exclude" && round($total / $month) >= 300 && $month <= $ms_bank_ktc){
                $select_bank_ktc .= '<div class="three col"><input type="hidden" id="payForMonth_ktc'.$month.'" value="'.round(($total / 100 * 0.8 * $month + $total) / $month,2).'"><input type="radio" name="permonth_ktc" id="perktc'.$month.'" value="'.$month.'" ><label for="perktc'.$month.'"><h4>'.number_format(($total / 100 * 0.8 * $month + $total) / $month,2).'  บาท / เดือน</h4></label></div>';
                if ($i_select_ktc == 4){
                    $select_bank_ktc .= '</div><div class="row cf">';
                    $i_select_ktc = 0;
                }
                $i_select_ktc++;
            }


        }

        $select_bank_ktc .= '</div>';

        $i_select_bay = 1;

        $select_bank_bay = '<div class="row cf">';

        foreach($BAY as $month){

        
            if ($ms_fee_type == "include" && round($total / $month) >= 500 && $month <= $ms_bank_bay){
                $select_bank_bay .= '<div class="three col"><input type="hidden" id="payForMonth_bay'.$month.'" value="'.round($total / $month,2).'"><input type="radio" name="permonth_bay" id="perbay'.$month.'" value="'.$month.'" ><label for="perbay'.$month.'"><h4>'.number_format($total / $month,2).'  บาท / เดือน</h4></label></div>';
                if ($i_select_bay == 4){
                    $select_bank_bay .= '</div><div class="row cf">';
                    $i_select_bay = 0;
                }
                $i_select_bay++;
            }else if ($ms_fee_type == "exclude" && round($total / $month) >= 500 && $month <= $ms_bank_bay){
                $select_bank_bay .= '<div class="three col"><input type="hidden" id="payForMonth_bay'.$month.'" value="'.round(($total / 100 * 0.8 * $month + $total) / $month,2).'"><input type="radio" name="permonth_bay" id="perbay'.$month.'" value="'.$month.'" ><label for="perbay'.$month.'"><h4>'.number_format(($total / 100 * 0.8 * $month + $total) / $month,2).'  บาท / เดือน</h4></label></div>';
                if ($i_select_bay == 4){
                    $select_bank_bay .= '</div><div class="row cf">';
                    $i_select_bay = 0;
                }
                $i_select_bay++;
            }


        }

        $select_bank_bay .= '</div>';

        $i_select_fcy = 1;

        $select_bank_fcy = '<div class="row cf">';

        foreach($FCY as $month){

        
            if ($ms_fee_type == "include" && round($total / $month) >= 300 && $month <= $ms_bank_fcy){
                $select_bank_fcy .= '<div class="three col"><input type="hidden" id="payForMonth_fcy'.$month.'" value="'.round($total / $month,2).'"><input type="radio" name="permonth_fcy" id="perfcy'.$month.'" value="'.$month.'" ><label for="perfcy'.$month.'"><h4>'.number_format($total / $month,2).'  บาท / เดือน</h4></label></div>';
                if ($i_select_fcy == 4){
                    $select_bank_fcy .= '</div><div class="row cf">';
                    $i_select_fcy = 0;
                }
                $i_select_fcy++;
            }else if ($ms_fee_type == "exclude" && round($total / $month) >= 300 && $month <= $ms_bank_fcy){
                $select_bank_fcy .= '<div class="three col"><input type="hidden" id="payForMonth_fcy'.$month.'" value="'.round(($total / 100 * 1 * $month + $total) / $month,2).'"><input type="radio" name="permonth_fcy" id="perfcy'.$month.'" value="'.$month.'" ><label for="perfcy'.$month.'"><h4>'.number_format(($total / 100 * 1 * $month + $total) / $month,2).'  บาท / เดือน</h4></label></div>';
                if ($i_select_fcy == 4){
                    $select_bank_fcy .= '</div><div class="row cf">';
                    $i_select_fcy = 0;
                }
                $i_select_fcy++;
            }


        }

        $select_bank_fcy .= '</div>';


        




        $result = $this->resultRawFactory->create();

        $forminstallment = '

        <meta charset="utf-8">
        <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <style>
        html, body{
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            font-family: "Prompt", sans-serif;
            background-color: #3498db;
          }
          
          h1, h2, h3, h4, h5 ,h6{
            font-weight: 200;
          }
          
          a{
            text-decoration: none;
          }
          
          p, li, a{
            font-size: 14px;
          }
          
          fieldset{
            margin: 0;
            padding: 0;
            border: none;
          }
          
          /* GRID */
          
          .twelve { width: 100%; }
          .eleven { width: 91.53%; }
          .ten { width: 83.06%; }
          .nine { width: 74.6%; }
          .eight { width: 66.13%; }
          .seven { width: 57.66%; }
          .six { width: 49.2%; }
          .five { width: 40.73%; }
          .four { width: 32.26%; }
          .three { width: 23.8%; }
          .two { width: 15.33%; }
          .one { width: 6.866%; }
          
          /* COLUMNS */
          
          .col {
              display: block;
              float:left;
              margin: 0 0 0 1.6%;
          }
          
          .col:first-of-type {
            margin-left: 0;
          }
          
          .container{
            width: 100%;
            max-width: 700px;
            margin: 0 auto;
            position: relative;
          }
          
          .row{
            padding: 20px 0;
          }
          
          /* CLEARFIX */
          
          .cf:before,
          .cf:after {
              content: " ";
              display: table;
          }
          
          .cf:after {
              clear: both;
          }
          
          .cf {
              *zoom: 1;
          }
          
          .wrapper{
            width: 100%;
            margin: 30px 0;
          }
          
          /* STEPS */
          
          .steps{
            list-style-type: none;
            margin: 0;
            padding: 0;
            background-color: #fff;
            text-align: center;
          }
          
          
          .steps li{
            display: inline-block;
            margin: 20px;
            color: #ccc;
            padding-bottom: 5px;
          }
          
          .steps li.is-active{
            border-bottom: 1px solid #3498db;
            color: #3498db;
          }
          
          /* FORM */
          
          .form-wrapper .section{
            padding: 0px 20px 30px 20px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            background-color: #fff;
            opacity: 0;
            -webkit-transform: scale(1, 0);
            -ms-transform: scale(1, 0);
            -o-transform: scale(1, 0);
            transform: scale(1, 0);
            -webkit-transform-origin: top center;
            -moz-transform-origin: top center;
            -ms-transform-origin: top center;
            -o-transform-origin: top center;
            transform-origin: top center;
            -webkit-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
            text-align: center;
            position: absolute;
            width: 100%;
            min-height: 300px
          }
          
          .form-wrapper .section h3{
            margin-bottom: 30px;
          }
          
          .form-wrapper .section.is-active{
            opacity: 1;
            -webkit-transform: scale(1, 1);
            -ms-transform: scale(1, 1);
            -o-transform: scale(1, 1);
            transform: scale(1, 1);
          }
          
          .form-wrapper .button, .form-wrapper .submit{
            background-color: #3498db;
            display: inline-block;
            padding: 8px 30px;
            color: #fff;
            cursor: pointer;
            font-size: 14px !important;
            font-family: "Prompt", sans-serif !important;
            position: absolute;
            right: 20px;
            bottom: 20px;
          }
          
          .form-wrapper .submit{
            border: none;
            outline: none;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
          }
          
          .form-wrapper input[type="text"],
          .form-wrapper input[type="password"]{
            display: block;
            padding: 10px;
            margin: 10px auto;
            background-color: #f1f1f1;
            border: none;
            width: 50%;
            outline: none;
            font-size: 14px !important;
            font-family: "Prompt", sans-serif !important;
          }
          
          .form-wrapper input[type="radio"]{
            display: none;
          }
          
          .form-wrapper input[type="radio"] + label{
            display: block;
            border: 1px solid #ccc;
            width: 100%;
            max-width: 100%;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            cursor: pointer;
            position: relative;
          }
          
          .form-wrapper input[type="radio"] + label:before{
            content: "✔";
            position: absolute;
            right: -10px;
            top: -10px;
            width: 30px;
            height: 30px;
            line-height: 30px;
            border-radius: 100%;
            background-color: #3498db;
            color: #fff;
            display: none;
          }
          
          .form-wrapper input[type="radio"]:checked + label:before{
            display: block;
          }
          
          .form-wrapper input[type="radio"] + label h4{
            margin: 15px;
            color: #ccc;
          }
          
          .form-wrapper input[type="radio"]:checked + label{
            border: 1px solid #3498db;
          }
          
          .form-wrapper input[type="radio"]:checked + label h4{
            color: #3498db;
          }
        </style>


        <script type="text/javascript">

        $(document).ready(function(){
            $("#banks").fadeIn(1000);
            $(".form-wrapper .button").click(function(){
              var button = $(this);
              var currentSection = button.parents(".section");
              var currentSectionIndex = currentSection.index();
              var headerSection = $(".steps li").eq(currentSectionIndex);

    
                currentSection.removeClass("is-active").next().addClass("is-active");
                headerSection.removeClass("is-active").next().addClass("is-active");
  
          
              $(".form-wrapper").submit(function(e) {
              
              });

              if (currentSectionIndex === 0) {
                CreatePayment($("input[name=bank]:checked").val())
                .then(data => {

                    var response = JSON.parse(data);
                    var status = response[0]["status"];
                    var transactionID = response[0]["Transaction ID"];

                    if(status === "Create Success"){

                        
                        $("#loading").remove();
                        $("#wait").hide();
                        $("#select_months").fadeIn(1000);

                        if($("input[name=bank]:checked").val() === "KTC"){
                            $("input:radio[name=permonth_ktc]")[0].checked = true;
                            $(".permonths_ktc").fadeIn(1000);
                            $("#transactionID").val(transactionID);
                            $("#bankType").val("KTC");
                            $("#next_bank").prop("disabled", false);
                            $("#next_bank").css("background-color", "#3498db");
                        }else if($("input[name=bank]:checked").val() === "BAY"){
                            $("input:radio[name=permonth_bay]")[0].checked = true;
                            $(".permonths_bay").fadeIn(1000);
                            $("#transactionID").val(transactionID);
                            $("#bankType").val("BAY");
                            $("#next_bank").prop("disabled", false);
                            $("#next_bank").css("background-color", "#3498db");
                        }else if($("input[name=bank]:checked").val() === "FCY"){
                            $("input:radio[name=permonth_fcy]")[0].checked = true;
                            $(".permonths_fcy").fadeIn(1000);
                            $("#transactionID").val(transactionID);
                            $("#bankType").val("FCY");
                            $("#next_bank").prop("disabled", false);
                            $("#next_bank").css("background-color", "#3498db");
                        }
                    }else{
                        $("#loading").remove();
                        $("#wait").hide();
                        $(".payment").append("<h3>Error : เกิดข้อผิดพลาดไม่สามารถชำระเงินได้ </h3>");
                    }
 
                    
                }).catch(error => {})
              }

              if(currentSectionIndex === 1){
                if($("input[name=bank]:checked").val() === "KTC"){
                    $("#term").val($("input[name=permonth_ktc]:checked").val());
                    $("#paymonth").val($("#payForMonth_ktc" + $("input[name=permonth_ktc]:checked").val()).val());
                    $("#payform").attr("action", "https://www.moneyspace.net/ktccredit/payment/directpay");
                        calfee('.round($total).',$("input[name=permonth_ktc]:checked").val(),"'.$ms_fee_type.'","ktc")
                    $("#paydesc").append("<h3>จำนวนเดือน : " + $("input[name=permonth_ktc]:checked").val() + " เดือน</h3>");
                    $("#paydesc").append("<h3>จำนวนเงินชำระต่อเดือน : " + numberWithCommas($("#payForMonth_ktc" + $("input[name=permonth_ktc]:checked").val()).val()) + " บาท / เดือน </h3>");


                }else if($("input[name=bank]:checked").val() === "BAY"){
                    $("#term").val($("input[name=permonth_bay]:checked").val());
                    $("#paymonth").val($("#payForMonth_bay" + $("input[name=permonth_bay]:checked").val()).val());
                    $("#payform").attr("action","https://www.moneyspace.net/baycredit/pay");
                    
                        calfee('.round($total).',$("input[name=permonth_bay]:checked").val(),"'.$ms_fee_type.'","bay")
                    $("#paydesc").append("<h3>จำนวนเดือน : " + $("input[name=permonth_bay]:checked").val() + " เดือน</h3>");
                    $("#paydesc").append("<h3>จำนวนเงินชำระต่อเดือน : " + numberWithCommas($("#payForMonth_bay" + $("input[name=permonth_bay]:checked").val()).val()) + " บาท / เดือน </h3>");
                    

                }else if($("input[name=bank]:checked").val() === "FCY"){
                    $("#term").val($("input[name=permonth_fcy]:checked").val());
                    $("#paymonth").val($("#payForMonth_fcy" + $("input[name=permonth_fcy]:checked").val()).val());
                    $("#payform").attr("action","https://www.moneyspace.net/baycredit/pay");


                        calfee('.round($total).',$("input[name=permonth_fcy]:checked").val(),"'.$ms_fee_type.'","fcy")
                    $("#paydesc").append("<h3>จำนวนเดือน : " + $("input[name=permonth_fcy]:checked").val() + " เดือน</h3>");
                    $("#paydesc").append("<h3>จำนวนเงินชำระต่อเดือน : " + numberWithCommas($("#payForMonth_fcy" + $("input[name=permonth_fcy]:checked").val()).val()) + " บาท / เดือน </h3>");
                }
              }

          
              if(currentSectionIndex === 3){
                $(document).find(".form-wrapper .section").first().addClass("is-active");
                $(document).find(".steps li").first().addClass("is-active");
              }
            });
          });

          function calfee(total,month,fee,bank){

            if(fee == "include"){
                $("#paydesc").append("<h3>ยอดเงิน : " + numberWithCommas(total.toFixed(2)) + " บาท</h3>");
            }

            if(fee == "exclude" && bank == "ktc"){
                var totalcal = parseInt(total) / 100 * 0.8 * parseInt(month) + parseInt(total)
                $("#paydesc").append("<h3>ยอดเงิน : " + numberWithCommas(totalcal.toFixed(2)) + " บาท (ดอกเบี้ย 0.8%)</h3>");
            }else if(fee == "exclude" && bank == "bay"){
                var totalcal = parseInt(total) / 100 * 0.8 * parseInt(month) + parseInt(total)
                $("#paydesc").append("<h3>ยอดเงิน : " + numberWithCommas(totalcal.toFixed(2)) + " บาท (ดอกเบี้ย 0.8%)</h3>");
            }else if(fee == "exclude" && bank == "fcy"){
                var totalcal = parseInt(total) / 100 * 1 * parseInt(month) + parseInt(total)
                $("#paydesc").append("<h3>ยอดเงิน : " + numberWithCommas(totalcal.toFixed(2)) + " บาท (ดอกเบี้ย 1%)</h3>");
            }
                
          }

          function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

          function CreatePayment(selectbank) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: '."'".$this->_url->getUrl('msinstallmentpayment/payment/installment')."'".',
                    type: "POST",
                    data: {
                        "bank": selectbank,
                    },
                    success: function(data) {
                        resolve(data)
                    },
                    error: function(error) {
                        reject(error)
                    },
                })
            })
        }
        
        </script>


        <div class="container">
            <div class="wrapper">
                <ul class="steps">
                    <li class="is-active">เลือกบัตรผ่อนชำระ</li>
                    <li>เลือกจำนวนเดือน</li>
                    <li>ยืนยัน</li>
                </ul>
                <div id="forminstallment" class="form-wrapper">
                    <fieldset class="section is-active" id="banks" style="display:none">
                    <div>
                    '.nl2br($desc_installment).'
                    <hr>
                    </div>
                    <div class="row cf">
                        '.$bank_ktc.$bank_bay1.$bank_fcy.$bank_bay2.'
                    </div>
                    <div class="row cf">
                        '.$bank_bay3.'
                    </div>
                        '.$btn_next.'
                    </fieldset>
                    <fieldset class="section">
                        
                        <div class="payment" style="text-align:center">
                            <h3 id="wait">กรุณารอสักครู่...</h3>
                            <img id="loading" src="http://smartboardpen.com/foto/data/loading2.gif" alt="">
                        </div>
                        <div id="select_months">
                        <div class="permonths_ktc" style="text-align:center;display:none">
                            <div>
                                    '.$select_bank_ktc.'
                            </div>
                        </div>
                        <div class="permonths_bay" style="text-align:center;display:none">
                            <div>
                                    '.$select_bank_bay.'
                            </div>
                        </div>
                        <div class="permonths_fcy" style="text-align:center;display:none">
                            <div>
                                    '.$select_bank_fcy.'
                            </div>
                        </div>
                        <button id="next_bank" class="button" style="background-color:#b2b3b4" disabled>ถัดไป</button>
                        </div>

                    </fieldset>
                    <fieldset class="section">

                    <div id="paydesc">

                    
                    </div>
                    

                    <form id="payform" method="post" action="">
                         <input type="hidden" id="transactionID" name="transactionID" value="">
                         <input type="hidden" id="pay_type" name="pay_type" value="">
                         <input type="hidden" id="locale" name="locale" value="">
                         <input type="hidden" id="term" name="term" value="">
                         <input type="hidden" id="paymonth" name="paymonth" value="">
                         <input type="hidden" id="bankType" name="bankType" value="">
                         <input type="hidden" id="interest" name="interest" value="0.0">
                         <button class="button" type="submit">ชำระเงิน</button>
                    </form>
                        
                    </fieldset>

                    <fieldset class="section">

                        <h3>กรุณารอสักครู่....</h3>

                    </fieldset>

                </div>
            </div>
         </div>
        ';

        if($ms_secret_id != "" && $ms_secret_key != ""){
            if($total >= 3000.01){
                $result->setContents($forminstallment);
            }else{
                $result->setContents('<div align="center"><h3>จำนวนยอดเงินต้อง <b style="color:red">3,000.01 บาท</b> ขึ้นไปถึงจะทำการผ่อนชำระได้</h3></div>');
            }
            
        }else{
            $result->setContents('<div align="center"><h3>กรุณาตั้งค่า Module</h3></div>');
        }
 
        
 
        return $result;

   
    }
}