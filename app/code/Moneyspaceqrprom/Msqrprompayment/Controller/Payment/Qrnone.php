<?php

namespace Moneyspaceqrprom\Msqrprompayment\Controller\Payment;

use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Quote\Model\Quote;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class Qrnone extends \Magento\Framework\App\Action\Action
{

    protected $_pageFactory;
    protected $_resultJsonFactory;
    protected $_checkoutSession;
    protected $orderRepository;
    protected $customerSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Sales\Model\Order $orderItemsDetails,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
        
    )
    {
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_pageFactory = $pageFactory;
        $this->orderRepository = $orderRepository;
        $this->customerSession = $customerSession;
        $this->resultRawFactory     = $resultRawFactory;
        return parent::__construct($context);
    }


    public function execute()
    {

        $customerId = $this->customerSession->getCustomer()->getId();
        $order = $this->_checkoutSession->getLastRealOrder();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderA = $objectManager->create('\Magento\Sales\Model\Order') ->load($order->getId());
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;


        /////////////////////////////////////////////////////

        $ms_secret_id = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/ms_secret_id',$storeScope);
        $ms_secret_key = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/ms_secret_key',$storeScope);
        $order_status_after_mspayment = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/moneyspaceqrprom/order_status_after_mspayment',$storeScope);
        

        ///////////////////////////////////////////////

        
            $timeHash = date("YmdHis");
            $hash_link = hash_hmac('sha256', $_GET["transaction_ID"].$timeHash, $ms_secret_key);
            $link = "https://www.moneyspace.net/merchantapi/makepayment/linkpaymentcard?transactionID=".$_GET["transaction_ID"]."&timehash=".$timeHash."&secreteID=".$ms_secret_id."&hash=".$hash_link;


            $RedirectURL_Success = "https://www.moneyspace.net/merchantapi/paycardsuccess";


            $result = $this->resultRawFactory->create();
 
            $result->setContents('
            <link href="//cdn.muicss.com/mui-0.10.1/css/mui.min.css" rel="stylesheet" type="text/css" />
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <br>
            <div class="mui-container-fluid">
                <div class="mui-row">
                    <div class="mui-col-md-6 mui-col-md-offset-3">
                        <div class="mui-panel" align="center">
                            <br>
                            <h3 id="scan">กรุณาสแกนชำระเงินด้วย Mobile Banking ภายในเวลา 15 นาที หลังชำระเงินกรุณารอสักครู่</h3>
                            <img id="qrpayment" src="'.$link.'">
                            <h3 id="successpayment" style="color:green;display:none">ชำระเงินเรียบร้อยแล้ว กรุณารอสักครู่...</h3>
                        </div>
                    </div>

                </div>    
            </div>
            <script type="text/javascript">
            $(document).ready(function () {
                setInterval(function () {
                    $.ajax({
                        url: "'.$this->_url->getUrl('msqrprompayment/redirect/callback').'",
                        data: {"transaction_ID": "'.$_GET["transaction_ID"].'"},
                        type: "POST",
                        success: function (data, textStatus, jqXHR) {
        
                            let res = JSON.parse(data);
        
                            if (res[0]["Status Payment "] == "Pay Success"){

                                $("#scan").hide();
                                $("#qrpayment").hide();
                                $("#successpayment").show();
        
                                window.location.href = "'.$RedirectURL_Success.'?Ref='.$_GET["transaction_ID"].'"

                            }else{
                                console.log(data)
                            }
        
                        }
                    });
                }, 1000);
            });        
            </script>
            ');
 
            return $result;

   
    }
}