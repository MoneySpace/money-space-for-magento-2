<?php
namespace Moneyspace\MoneyspacePayment\Model\Config\Payment;
 
class Fee implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'include', 'label' => __('ร้านค้ารับผิดชอบค่าธรรมเนียมบัตรเครดิต /เดบิต')],
            ['value' => 'exclude', 'label' => __('ผู้ซื้อรับผิดชอบค่าธรรมเนียมบัตรเครดิต /เดบิต')],
        ];
    }
}