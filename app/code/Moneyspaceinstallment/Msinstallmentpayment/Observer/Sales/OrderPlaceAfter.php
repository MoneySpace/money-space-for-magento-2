<?php
/**
 * Copyright © MoneySpace All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Moneyspaceinstallment\Msinstallmentpayment\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class OrderPlaceAfter implements \Magento\Framework\Event\ObserverInterface
{

    protected $logger;
 
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        //Your observer code
        try {
            $order = $observer->getEvent()->getOrder();
            $payment = $order->getPayment();
            $method = $payment->getMethodInstance();
            $order_status = $method->getConfigData("order_status");
            if ($order_status == "pending") {
                $order->setStatus($order_status);
                $order->setState(Order::STATE_NEW);
            }
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }
}
