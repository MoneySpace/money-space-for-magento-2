<?php
namespace Moneyspaceinstallment\Msinstallmentpayment\Model\Config\Installment;
 
class Feeinstallment implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'include', 'label' => __('ร้านค้ารับผิดชอบดอกเบี้ยรายเดือน')],
            ['value' => 'exclude', 'label' => __('ผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน ( ดอกเบี้ย : 0.8% , 1% )')],
        ];
    }
}