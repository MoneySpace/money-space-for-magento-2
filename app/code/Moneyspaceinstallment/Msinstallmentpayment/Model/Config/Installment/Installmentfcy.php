<?php
namespace Moneyspaceinstallment\Msinstallmentpayment\Model\Config\Installment;
 
class Installmentfcy implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => '0', 'label' => __('ปิด')],
            ['value' => '3', 'label' => __('3 เดือน')],
            ['value' => '4', 'label' => __('4 เดือน')],
            ['value' => '6', 'label' => __('6 เดือน')],
            ['value' => '9', 'label' => __('9 เดือน')],
            ['value' => '10', 'label' => __('10 เดือน')],
            ['value' => '12', 'label' => __('12 เดือน (เฉพาะผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน)')],
            ['value' => '18', 'label' => __('18 เดือน (เฉพาะผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน)')],
            ['value' => '24', 'label' => __('24 เดือน (เฉพาะผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน)')],
            ['value' => '36', 'label' => __('36 เดือน (เฉพาะผู้ถือบัตรรับผิดชอบดอกเบี้ยรายเดือน)')]
        ];
    }
}